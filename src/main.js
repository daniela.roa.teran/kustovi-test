import Vue from "vue";
import firebase from "firebase";
import App from "./App.vue";

Vue.config.productionTip = false;

var firebaseConfig = {
  apiKey: "AIzaSyCWv0mzDMWxuTPQ8xBrZqsq2b_Nc-wewkk",
  authDomain: "kustovi-test.firebaseapp.com",
  databaseURL: "https://kustovi-test.firebaseio.com",
  projectId: "kustovi-test",
  storageBucket: "kustovi-test.appspot.com",
  messagingSenderId: "930218873054",
  appId: "1:930218873054:web:782dd762825bab74ca724f",
  measurementId: "G-HLQJFMV3H8",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.functions().useFunctionsEmulator("http://localhost:8080");
firebase.analytics();

new Vue({
  render: (h) => h(App),
}).$mount("#app");
